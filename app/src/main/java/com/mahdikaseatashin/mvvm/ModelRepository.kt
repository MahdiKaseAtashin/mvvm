package com.mahdikaseatashin.mvvm

import android.app.Application
import androidx.lifecycle.LiveData

class ModelRepository(application: Application) {
    private var modelDao : ModelDao
    private var allModels : LiveData<List<Model>>
    private val database = ModelDatabase.getInstance(application)

    init {
        modelDao = database.modelDao()
        allModels = modelDao.getAllModels()
    }

    fun insert(model: Model) {
        subscribeOnBackground {
            modelDao.insert(model)
        }
    }

    fun update(model: Model) {
        subscribeOnBackground {
            modelDao.update(model)
        }
    }

    fun delete(model: Model) {
        subscribeOnBackground {
            modelDao.delete(model)
        }
    }

    fun getAllModels(): LiveData<List<Model>> {
        return allModels
    }

}
