package com.mahdikaseatashin.mvvm

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData

class ModelViewModel(application : Application) : AndroidViewModel(application) {
    private val repository = ModelRepository(application)
    private val allModels = repository.getAllModels()

    fun insert(model: Model) {
        repository.insert(model)
    }

    fun update(model: Model) {
        repository.update(model)
    }

    fun delete(model: Model) {
        repository.delete(model)
    }


    fun getAllNotes(): LiveData<List<Model>> {
        return allModels
    }
}
