package com.mahdikaseatashin.mvvm

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.sqlite.db.SupportSQLiteDatabase

@Database(entities = [Model::class], version = 1)
abstract class ModelDatabase : RoomDatabase() {
    abstract fun modelDao(): ModelDao

    companion object {
        private var instance: ModelDatabase? = null

        @Synchronized
        fun getInstance(ctx: Context): ModelDatabase {
            if (instance == null)
                instance = Room.databaseBuilder(
                    ctx.applicationContext, ModelDatabase::class.java,
                    "note_database"
                )
                    .fallbackToDestructiveMigration()
                    .addCallback(roomCallback)
                    .build()

            return instance!!

        }

        private val roomCallback = object : Callback() {
            override fun onCreate(db: SupportSQLiteDatabase) {
                super.onCreate(db)
                populateDatabase(instance!!)
            }
        }
        private fun populateDatabase(db: ModelDatabase) {
            val modelDao = db.modelDao()
            subscribeOnBackground {
                modelDao.insert(Model(null, "title 1", "desc 1"))
            }
        }
    }

}
